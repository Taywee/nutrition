use std::{fmt, io};

#[derive(Debug)]
pub enum Error {
    Rusqlite(rusqlite::Error),
    WrongApplicationId,
    NoSuchInode(u64),
    InvalidHandle,
    NotFile,
    NotFound,
    NotLink,
    NotDirectory,
    AlreadyExists(u64),
    PositionTooBig,
    PositionNegative,
    FilesystemClosed,
}

impl From<rusqlite::Error> for Error {
    fn from(v: rusqlite::Error) -> Self {
        Self::Rusqlite(v)
    }
}

impl std::error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Rusqlite(e) => write!(f, "Rusqlite: {}", e),
            Error::WrongApplicationId => write!(f, "WrongApplicationId"),
            Error::NoSuchInode(inode) => write!(f, "NoSuchInode: {}", inode),
            Error::InvalidHandle => write!(f, "InvalidHandle"),
            Error::NotFile => write!(f, "NotFile"),
            Error::NotFound => write!(f, "NotFound"),
            Error::NotLink => write!(f, "NotLink"),
            Error::NotDirectory => write!(f, "NotDirectory"),
            Error::AlreadyExists(inode) => write!(f, "AlreadyExists: {}", inode),
            Error::PositionTooBig => write!(f, "PositionTooBig"),
            Error::PositionNegative => write!(f, "PositionNegative"),
            Error::FilesystemClosed => write!(f, "FilesystemClosed"),
        }
    }
}

impl From<Error> for io::Error {
    fn from(error: Error) -> Self {
        io::Error::new(io::ErrorKind::Other, error)
    }
}
