use crate::ui::add_food;

use adw::{
    gtk::{Builder, Button},
    prelude::{ButtonExt, IsA},
};
use nutrition::database::Database;

use std::{
    cell::RefCell,
    path::PathBuf,
    pin::Pin,
    rc::{Rc, Weak},
};

use adw::gtk::Box;
use adw::ApplicationWindow;

use clap::Parser;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Path to the nutrition database.
    #[clap(short, long)]
    database: PathBuf,
}

pub fn main_window<T>(application: &T, database: Weak<RefCell<Database>>) -> ApplicationWindow
where
    T: IsA<adw::gtk::Application>,
{
    let builder = Builder::from_string(include_str!("main_window.ui"));
    let box_: Box = builder.object("box").expect("UI needs the main box");
    let add_food_button: Button = builder
        .object("add-food-button")
        .expect("UI needs the add food button");

    let window = ApplicationWindow::builder()
        .application(application)
        .default_width(350)
        .content(&box_)
        .build();

    {
        let window = window.clone();
        let database = database.clone();
        add_food_button.connect_clicked(move |_| {
            let database = database.clone();
            add_food(database, &window);
        });
    }

    window
}
