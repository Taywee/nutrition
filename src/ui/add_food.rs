use rusqlite::params;
use std::{
    cell::RefCell,
    pin::Pin,
    rc::{Rc, Weak},
};
use tracing::{event, Level};

use adw::{
    gtk::{
        Builder, Dialog, DropDown, Entry, EntryBuffer, MessageDialog, ResponseType, StringList,
        Window,
    },
    prelude::{DialogExt, EntryBufferExtManual, EntryExt, GtkWindowExt, IsA, WidgetExt},
};
use nutrition::{
    database::{Database, Savepoint},
    include_sql,
};

struct AddFoodDialog {
    identifier_entry: Entry,
    name_entry: Entry,
    description_entry: Entry,
    serving_entry: Entry,
    serving_unit_dropdown: DropDown,
    calories_entry: Entry,
    unit_names: Vec<String>,
    savepoint: Savepoint,
}
impl AddFoodDialog {
    pub fn new<T>(database: Weak<RefCell<Database>>, parent: &T) -> (Self, Dialog)
    where
        T: IsA<Window>,
    {
        let mut savepoint =
            Savepoint::new(database.upgrade().expect("Database should not be expired"))
                .expect("Could not open savepoint");
        let builder = Builder::from_string(include_str!("add_food.ui"));
        let dialog: Dialog = builder
            .object("dialog")
            .expect("Could not find dialog in UI");
        let identifier_entry: Entry = builder
            .object("identifier")
            .expect("Could not find identifier in UI");
        let name_entry: Entry = builder.object("name").expect("Could not find name in UI");
        let description_entry: Entry = builder
            .object("description")
            .expect("Could not find description in UI");
        let serving_entry: Entry = builder
            .object("serving")
            .expect("Could not find serving in UI");
        let serving_unit_dropdown: DropDown = builder
            .object("serving-unit")
            .expect("Could not find serving-unit in UI");
        let calories_entry: Entry = builder
            .object("calories")
            .expect("Could not find calories in UI");

        let unit_names = {
            let mut statement = savepoint
                .prepare_cached("SELECT name FROM units")
                .expect("select name from units");
            let unit_strings: Result<Vec<String>, _> = statement
                .query_map([], |row| row.get(0))
                .expect("query_row unit names")
                .collect();
            unit_strings.expect("All units must have valid rows")
        };

        let unit_strings: Vec<_> = unit_names.iter().map(String::as_str).collect();
        let stringlist = StringList::new(&unit_strings);
        serving_unit_dropdown.set_model(Some(&stringlist));

        dialog.set_transient_for(Some(parent));

        (
            Self {
                identifier_entry,
                name_entry,
                description_entry,
                serving_entry,
                serving_unit_dropdown,
                calories_entry,
                unit_names,
                savepoint,
            },
            dialog,
        )
    }

    fn accept(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        fn non_empty_buffer(buffer: EntryBuffer) -> Option<String> {
            let buffer = buffer.text();
            if buffer.is_empty() {
                None
            } else {
                Some(buffer)
            }
        }

        let identifier = non_empty_buffer(self.identifier_entry.buffer());
        let name = self.name_entry.buffer().text();
        let description = non_empty_buffer(self.description_entry.buffer());

        let serving: f32 = self.serving_entry.buffer().text().parse()?;

        let serving_unit: &str = {
            let serving_unit_index = self.serving_unit_dropdown.selected() as usize;
            &self
                .unit_names
                .get(serving_unit_index)
                .ok_or_else(|| String::from("Serving unit out of place"))?
        };

        let calories: i64 = self.calories_entry.buffer().text().parse()?;

        {
            let mut statement = self.savepoint.prepare_cached(include_sql!("insert_food"))?;
            statement.execute(params![
                identifier,
                name,
                description,
                serving,
                serving_unit,
                calories
            ])?;
        }

        self.savepoint.commit()?;
        Ok(self.savepoint.commit()?)
    }
}

/// Open the add food dialog and get it ready to add a food item to the database.
pub fn add_food<T>(database: Weak<RefCell<Database>>, parent: &T) -> Dialog
where
    T: IsA<Window>,
{
    let (add_food, dialog) = AddFoodDialog::new(database, parent);
    let add_food = RefCell::new(add_food);
    dialog.connect_response(move |dialog, response_type| match response_type {
        ResponseType::Accept => match add_food.borrow_mut().accept() {
            Ok(()) => {
                dialog.close();
            }
            Err(e) => {
                let message_dialog = MessageDialog::builder()
                    .buttons(adw::gtk::ButtonsType::Ok)
                    .message_type(adw::gtk::MessageType::Error)
                    .title("Error creating food")
                    .text("Error creating food")
                    .secondary_text(&e.to_string())
                    .modal(true)
                    .transient_for(dialog)
                    .build();
                message_dialog.connect_response(move |dialog, response_type| match response_type {
                    ResponseType::Ok => {
                        dialog.close();
                    }
                    ResponseType::Close => (),
                    ResponseType::DeleteEvent => (),
                    e => panic!("Unknown response: {:?}", e),
                });
                message_dialog.show();
            }
        },
        ResponseType::Cancel => {
            dialog.close();
        }
        ResponseType::Close => (),
        ResponseType::DeleteEvent => (),
        e => panic!("Unknown response: {:?}", e),
    });
    dialog.show();
    dialog
}
