use crate::{error::Error, include_sql};
use std::{
    cell::{RefCell, RefMut},
    ops::{Deref, DerefMut},
    path::Path,
    pin::Pin,
    rc::Rc,
};

use rusqlite::{Connection, OpenFlags, TransactionBehavior};
use tracing::{event, Level};

/// A savepoint that holds its RefCell open for the duration of its run, and keeps a
/// mutable self-referencing savepoint.  This is necessary to work around borrow checker
/// restrictions, in regard to holding a savepoint open for the reference counted duration.
pub struct Savepoint {
    // The savepoint counts as a mutable reference.  The database may not be mutably aliased
    // anywhere else, and RefCell ensures this.
    savepoint: Option<rusqlite::Savepoint<'static>>,
    _database: RefMut<'static, Database>,
    _database_rc: Rc<RefCell<Database>>,
}

impl Savepoint {
    pub fn new(database: Rc<RefCell<Database>>) -> rusqlite::Result<Self> {
        let _database_rc = database.clone();
        unsafe {
            let mut ref_mut: RefMut<'static, Database> = std::mem::transmute(database.borrow_mut());
            let savepoint: rusqlite::Savepoint<'static> = std::mem::transmute(ref_mut.savepoint()?);
            Ok(Self {
                _database_rc,
                savepoint: Some(savepoint),
                _database: ref_mut,
            })
        }
    }

    pub fn commit(&mut self) -> rusqlite::Result<()> {
        if let Some(savepoint) = self.savepoint.take() {
            event!(Level::TRACE, "Committing savepoint");
            savepoint.commit()
        } else {
            Ok(())
        }
    }

    pub fn rollback(&mut self) -> rusqlite::Result<()> {
        if let Some(savepoint) = self.savepoint.as_mut() {
            event!(Level::TRACE, "Rolling back savepoint");
            savepoint.rollback()
        } else {
            Ok(())
        }
    }

    pub fn finish(&mut self) -> rusqlite::Result<()> {
        if let Some(savepoint) = self.savepoint.take() {
            event!(Level::TRACE, "Finishing savepoint");
            savepoint.finish()
        } else {
            Ok(())
        }
    }
}

impl Deref for Savepoint {
    type Target = rusqlite::Savepoint<'static>;

    fn deref(&self) -> &Self::Target {
        self.savepoint
            .as_ref()
            .expect("Can not deref a committed savepoint")
    }
}

impl DerefMut for Savepoint {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.savepoint
            .as_mut()
            .expect("Can not deref_mut a committed savepoint")
    }
}
impl Drop for Savepoint {
    fn drop(&mut self) {
        if let Some(_savepoint) = self.savepoint.take() {
            event!(Level::TRACE, "Dropping savepoint");
        }
    }
}

#[derive(Debug)]
pub struct Database {
    connection: Connection,
}

impl Deref for Database {
    type Target = Connection;

    fn deref(&self) -> &Self::Target {
        &self.connection
    }
}

impl DerefMut for Database {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.connection
    }
}

impl Database {
    pub fn new<P>(path: P) -> Result<Self, Error>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        let mut connection = Connection::open_with_flags(
            path,
            OpenFlags::SQLITE_OPEN_READ_WRITE
                | OpenFlags::SQLITE_OPEN_CREATE
                | OpenFlags::SQLITE_OPEN_EXRESCODE
                | OpenFlags::SQLITE_OPEN_PRIVATE_CACHE
                | OpenFlags::SQLITE_OPEN_NO_MUTEX,
        )?;
        connection.execute_batch(include_sql!("pragmas"))?;

        let application_id: i32 =
            connection.query_row("PRAGMA application_id", [], |row| row.get(0))?;

        match application_id {
            0 => {
                connection.execute(
                    &format!("PRAGMA application_id = {}", crate::APPLICATION_ID),
                    [],
                )?;
            }
            crate::APPLICATION_ID => (),
            _ => return Err(Error::WrongApplicationId),
        }

        let transaction = connection.transaction_with_behavior(TransactionBehavior::Exclusive)?;

        transaction.execute(include_sql!("create_table_metadata"), [])?;

        transaction.execute(
            "INSERT INTO metadata (id) VALUES (1) ON CONFLICT DO NOTHING",
            [],
        )?;

        let start_database_version: u64 =
            transaction.query_row("SELECT database_version FROM metadata", [], |row| {
                row.get(0)
            })?;

        let mut database_version = start_database_version;

        if database_version < 1 {
            transaction.execute_batch(include_sql!("initialize_version_1"))?;

            database_version = 1;
        }

        if database_version != start_database_version {
            transaction.execute("UPDATE metadata SET database_version=?", [database_version])?;
        }

        transaction.commit()?;
        Ok(Self { connection })
    }
}

impl Drop for Database {
    fn drop(&mut self) {
        event!(Level::TRACE, "Closing database");
        self.execute("PRAGMA optimize", [])
            .expect("Could not optimize database");
    }
}
