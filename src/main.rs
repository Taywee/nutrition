mod ui;

use std::cell::RefCell;
use std::path::PathBuf;
use std::rc::Rc;

use adw::gtk::Application;
use adw::prelude::*;

use clap::Parser;

use nutrition::database::Database;
use nutrition::Error;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Path to the nutrition database.
    #[clap(short, long)]
    database: PathBuf,
}

fn main() -> Result<(), Error> {
    tracing_subscriber::fmt::init();

    let args = Args::parse();

    let database = Rc::new(RefCell::new(Database::new(args.database)?));

    let application = Application::builder()
        .application_id("net.axfive.Nutrition")
        .build();

    application.connect_startup(|_| {
        adw::init();
    });

    let database = Rc::downgrade(&database);
    application.connect_activate(move |app| {
        ui::main_window(app, database.clone()).show();
    });

    application.run_with_args::<&str>(&[]);

    Ok(())
}
