INSERT INTO food (
    identifier,
    name,
    description,
    serving,
    serving_unit,
    calories,
    nutrients
)
VALUES (
    ?,
    ?,
    ?,
    ?,
    (SELECT id FROM units WHERE name = ?),
    ?,
    '{}'
)
