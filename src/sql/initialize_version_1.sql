-- Table of units, for suffixes and conversions.
-- Non-suffix units are created with the default empty unit.
CREATE TABLE units (
    id INTEGER PRIMARY KEY,
    name TEXT UNIQUE NOT NULL,
    suffix TEXT
);

CREATE TABLE food (
    id INTEGER PRIMARY KEY,

    identifier TEXT UNIQUE CHECK (identifier IS NULL OR identifier != ''),
    name TEXT NOT NULL CHECK (name != ''),
    description TEXT CHECK (description IS NULL OR description != ''),

    serving REAL NOT NULL,
    serving_unit INTEGER NOT NULL REFERENCES units(id) ON DELETE RESTRICT,
    calories INTEGER NOT NULL,

    -- JSON nutrient counts
    nutrients TEXT NOT NULL
);

-- A conversion between two units, possibly specific to a particular food item
-- (to account for different densities or unit counts).  Without a food item,
-- conversions are universal, so they should be different units of the same
-- dimension, like grams and ounces.
CREATE TABLE unit_conversions (
    id INTEGER PRIMARY KEY,
    from_unit INTEGER NOT NULL REFERENCES units(id) ON DELETE CASCADE,
    to_unit INTEGER NOT NULL REFERENCES units(id) ON DELETE CASCADE,
    food INTEGER REFERENCES food(id) ON DELETE CASCADE,

    -- We multiply by from_unit to get to_unit
    multiplier REAL NOT NULL,

    -- conversions are bidirectional, so we force this.
    CHECK (from_unit < to_unit)
);

CREATE UNIQUE INDEX unit_conversions_index ON unit_conversions(from_unit, to_unit, food);

-- A view that conveniently fixes the order on selections and insertions.
CREATE VIEW unit_conversions_view (id, from_unit, to_unit, food, multiplier) AS
SELECT id, from_unit, to_unit, food, multiplier FROM unit_conversions
UNION ALL SELECT id, to_unit, from_unit, food, 1.0 / multiplier FROM unit_conversions;

CREATE TRIGGER unit_conversions_view_insert INSTEAD OF INSERT ON unit_conversions_view
BEGIN
    INSERT INTO unit_conversions (from_unit, to_unit, food, multiplier) VALUES (
        MIN(NEW.from_unit, NEW.to_unit),
        MAX(NEW.from_unit, NEW.to_unit),
        NEW.food,
        IIF(NEW.from_unit < NEW.to_unit, NEW.multiplier, 1.0 / NEW.multiplier));
END;

CREATE TRIGGER unit_conversions_view_delete INSTEAD OF DELETE ON unit_conversions_view
BEGIN
    DELETE FROM unit_conversions WHERE id=OLD.id;
END;

-- Combined foods, gives ingredients to be components of a top-level food dish.
CREATE TABLE ingredients (
    id INTEGER PRIMARY KEY,
    food INTEGER NOT NULL REFERENCES food(id) ON DELETE CASCADE,
    ingredient INTEGER NOT NULL REFERENCES food(id) ON DELETE CASCADE,
    amount REAL NOT NULL,
    amount_unit INTEGER NOT NULL REFERENCES units(id) ON DELETE RESTRICT
);

CREATE UNIQUE INDEX ingredients_index ON ingredients(food, ingredient);

-- The actual table of things you ate and when you ate them.
CREATE TABLE diet (
    id INTEGER PRIMARY KEY,
    time INTEGER NOT NULL,
    food INTEGER NOT NULL REFERENCES food(id) ON DELETE CASCADE,
    portion REAL NOT NULL,
    portion_unit INTEGER NOT NULL REFERENCES units(id) ON DELETE RESTRICT
);

CREATE INDEX diet_time_index ON diet(time ASC);

--CREATE TABLE exercise (
    --  id INTEGER PRIMARY KEY,
    --  name TEXT UNIQUE NOT NULL,
    --  description TEXT,
    --  calories_per_hour INTEGER NOT NULL
    --);
--
--CREATE TABLE workout (
    --  id INTEGER PRIMARY KEY,
    --  time INTEGER NOT NULL,
    --  minutes INTEGER NOT NULL,
    --  exercise INTEGER NOT NULL REFERENCES exercise(id) ON DELETE CASCADE
    --);

INSERT INTO units (name, suffix) VALUES
    -- The base unit
    ('', NULL),

    -- mass
    -- metric
    ('milligram', 'mg'),
    ('gram', 'g'),
    ('kilogram', 'kg'),

    -- US
    ('ounce', 'oz'),
    ('pound', 'lb'),

    -- volume
    -- metric
    ('milliliter', 'ml'),
    ('liter', 'l'),
    ('cubic centimeter', 'cc'),
    ('imperial gallon', 'imp gal'),

    -- US
    ('teaspoon', 'tsp'),
    ('tablespoon', 'tbsp'),
    ('fluid ounce', 'fl oz'),
    ('cup', NULL),
    ('pint', 'pt'),
    ('quart', 'qt'),
    ('US gallon', 'US gal'),
    ('US dry gallon', 'usdrygal');

INSERT INTO unit_conversions_view (from_unit, to_unit, multiplier) VALUES
    (
        (SELECT id FROM units WHERE name='kilogram'),
        (SELECT id FROM units WHERE name='gram'),
        1000),
    (
        (SELECT id FROM units WHERE name='gram'),
        (SELECT id FROM units WHERE name='milligram'),
        1000),
    (
        (SELECT id FROM units WHERE name='ounce'),
        (SELECT id FROM units WHERE name='gram'),
        28.34952313),
    (
        (SELECT id FROM units WHERE name='pound'),
        (SELECT id FROM units WHERE name='ounce'),
        16),
    (
        (SELECT id FROM units WHERE name='liter'),
        (SELECT id FROM units WHERE name='milliliter'),
        1000),
    (
        (SELECT id FROM units WHERE name='cubic centimeter'),
        (SELECT id FROM units WHERE name='milliliter'),
        1),
    (
        (SELECT id FROM units WHERE name='teaspoon'),
        (SELECT id FROM units WHERE name='milliliter'),
        4.928921594),
    (
        (SELECT id FROM units WHERE name='tablespoon'),
        (SELECT id FROM units WHERE name='teaspoon'),
        3),
    (
        (SELECT id FROM units WHERE name='fluid ounce'),
        (SELECT id FROM units WHERE name='tablespoon'),
        2),
    (
        (SELECT id FROM units WHERE name='cup'),
        (SELECT id FROM units WHERE name='fluid ounce'),
        8),
    (
        (SELECT id FROM units WHERE name='pint'),
        (SELECT id FROM units WHERE name='cup'),
        2),
    (
        (SELECT id FROM units WHERE name='quart'),
        (SELECT id FROM units WHERE name='pint'),
        2),
    (
        (SELECT id FROM units WHERE name='US gallon'),
        (SELECT id FROM units WHERE name='quart'),
        4),
    (
        (SELECT id FROM units WHERE name='US dry gallon'),
        (SELECT id FROM units WHERE name='liter'),
        4.40488377086),
    (
        (SELECT id FROM units WHERE name='imperial gallon'),
        (SELECT id FROM units WHERE name='liter'),
        4.54609);
