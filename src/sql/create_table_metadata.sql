CREATE TABLE IF NOT EXISTS metadata (
    id INTEGER PRIMARY KEY NOT NULL CHECK (id = 1),
    database_version INTEGER NOT NULL DEFAULT 0
)
