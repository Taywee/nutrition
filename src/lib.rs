pub mod database;
mod error;
pub use error::Error;

const APPLICATION_ID: i32 = 1397067721;

#[macro_export]
macro_rules! include_sql {
    ($name:expr) => {
        include_str!(concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/src/sql/",
            $name,
            ".sql"
        ))
    };
}
