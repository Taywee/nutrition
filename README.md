# nutrition

A nutrition counter for calories, macronutrients, micronutrients, and exercise.  May eventually encompass goal setting and tracking and TDEE, but at this point, it's a pretty simple and bog-standard tracker.
